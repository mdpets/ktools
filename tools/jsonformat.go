package tools

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
)

func JsonFormat(jsonStr string) (string, error) {
	resp := ""
	jsonStrTemp := ""
	arr := strings.Split(jsonStr, "\r\n")
	if len(arr) > 0 {
		data := make([]string, 0)
		for _, def := range arr {
			def = strings.Trim(def, " ")
			if def == "" {
				continue
			}
			data = append(data, def)
		}
		for key, def := range data {
			def = strings.Trim(def, " ")
			if def == "" {
				continue
			}
			def = strings.ReplaceAll(def, "://", "")
			temp := strings.Split(def, "#")
			temp = strings.Split(temp[0], "//")
			if temp[0] == "" {
				continue
			}
			kv := strings.ReplaceAll(temp[0], "”", "\"")
			kv = strings.ReplaceAll(kv, "“", "\"")
			kv = strings.ReplaceAll(kv, "，", ",")
			kv = strings.ReplaceAll(kv, "：", ":")
			kv = strings.ReplaceAll(kv, " ", "")
			if kv == "{" || kv == "}" {
				jsonStrTemp = fmt.Sprintf("%s%s", jsonStrTemp, kv)
				jsonStrTemp = strings.ReplaceAll(jsonStrTemp, ",}", "}")
				jsonStrTemp = strings.ReplaceAll(jsonStrTemp, "}\"", "},\"")
				continue
			}
			if key < len(data)-2 && !strings.Contains(data[key], "{") && !strings.Contains(data[key], "[") && !strings.Contains(data[key+1], "}") && kv[len(kv)-1:len(kv)] != "," {
				kv = fmt.Sprintf("%s,", kv)
			}
			kv = strings.ReplaceAll(kv, "\t", "")
			kv = strings.ReplaceAll(kv, "\r", "")
			kv = strings.ReplaceAll(kv, ",}", "}")
			kv = strings.ReplaceAll(kv, "}\"", "},\"")
			jsonStrTemp = fmt.Sprintf("%s%s", jsonStrTemp, kv)
			jsonStrTemp = strings.ReplaceAll(jsonStrTemp, ",}", "}")
			jsonStrTemp = strings.ReplaceAll(jsonStrTemp, "}\"", "},\"")
		}
	}
	if len(jsonStrTemp) > 0 {
		var value interface{}
		err := json.Unmarshal([]byte(jsonStrTemp), &value)
		if err != nil {
			return "", err
		}

		var str bytes.Buffer
		_ = json.Indent(&str, []byte(jsonStrTemp), "\r\n", "\t")
		resp = str.String()
	}
	return resp, nil
}
