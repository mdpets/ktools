package tools

import (
	"container/list"
	"fmt"
	"reflect"
	"strings"

	"github.com/iancoleman/strcase"
)

// parseJSONObjects parses the JSON objects and constructs the final struct.
func parseJSONObjects(l *list.List, e map[string]interface{}, remark map[string]string, tabCount int) {
	for key, value := range e {
		key = strings.Trim(key, " ")
		switch val := value.(type) {
		case string:
			if _, ok := remark[key]; ok {
				l.PushBack(fmt.Sprintf("%s%-14s %s\t%s%s\r\n", printTabs(tabCount), strcase.ToCamel(key), "string", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`", remark[key]))
			} else {
				l.PushBack(fmt.Sprintf("%s%-14s %s\t%s\r\n", printTabs(tabCount), strcase.ToCamel(key), "string", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`"))
			}
		case []interface{}:
			var tabs int
			if tabCount%2 == 0 {
				if _, ok := remark[key]; ok {
					l.PushBack(fmt.Sprintf("%s%s []struct { %s\r\n", printTabs(tabCount), strcase.ToCamel(key), remark[key]))
				} else {
					l.PushBack(fmt.Sprintf("%s%s []struct {\r\n", printTabs(tabCount), strcase.ToCamel(key)))
				}
				tabs = tabCount
				tabCount += 2
			}
			t, length := parseJSONArrays(l, val, remark, 1, tabCount)
			if len(t) != 0 {
				dataType := getType(t, length)
				if _, ok := remark[key]; ok {
					l.PushBack(fmt.Sprintf("%s%-14s %s\t%s%s\r\n", printTabs(tabCount), strcase.ToCamel(key), dataType, "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`", remark[key]))
				} else {
					l.PushBack(fmt.Sprintf("%s%-14s %s\t%s", printTabs(tabCount), strcase.ToCamel(key), dataType, "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`\r\n"))
				}
			}
			if tabs%2 == 0 {
				first = false
				if _, ok := remark[key]; ok {
					l.PushBack(fmt.Sprintf("%s%s %s%s\r\n", printTabs(tabs), "}", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`", remark[key]))
				} else {
					l.PushBack(fmt.Sprintf("%s%s %s", printTabs(tabs), "}", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`\r\n"))
				}
			}
		case float64:
			if _, ok := remark[key]; ok {
				l.PushBack(fmt.Sprintf("%s%-14s %s\t%s%s\r\n", printTabs(tabCount), strcase.ToCamel(key), "int", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`", remark[key]))
			} else {
				l.PushBack(fmt.Sprintf("%s%-14s %s\t%s", printTabs(tabCount), strcase.ToCamel(key), "int", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`\r\n"))
			}
		case map[string]interface{}:
			if _, ok := remark[key]; ok {
				l.PushBack(fmt.Sprintf("%s%s struct { %s\r\n", printTabs(tabCount), strcase.ToCamel(key), remark[key]))
			} else {
				l.PushBack(fmt.Sprintf("%s%s struct { \r\n", printTabs(tabCount), strcase.ToCamel(key)))
			}
			parseJSONObjects(l, val, remark, tabCount+2)
			if _, ok := remark[key]; ok {
				l.PushBack(fmt.Sprintf("%s%s %s%s\r\n", printTabs(tabCount), "}", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`", remark[key]))
			} else {
				l.PushBack(fmt.Sprintf("%s%s %s", printTabs(tabCount), "}", "`schema:\""+key+"\" url:\""+key+"\" json:\""+key+"\"`\r\n"))
			}
		default:
			fmt.Println(reflect.TypeOf(val))
			continue
		}

	}
}

// parseJSONArrays parses the JSON arrys and calls the JSON object parser function for further parsing.
func parseJSONArrays(l *list.List, e []interface{}, remark map[string]string, level, tabCount int) ([]string, int) {
	dataTypes := make([]string, 0)
	counter := make(map[string]int, 0)
	lastKey := ""
	if len(e) == 0 {
		return dataTypes, level
	}
	for i, value := range e {
		switch val := value.(type) {
		case float64:
			dataTypes = append(dataTypes, "int")
		case string:
			dataTypes = append(dataTypes, "string")
		case []interface{}:
			if i == 0 {
				level++
			}
			dataTypes, level = parseJSONArrays(l, val, remark, level, tabCount)
		case map[string]interface{}:
			for key := range val {
				counter[key]++
				lastKey = key
			}
			if counter[lastKey] > 1 {
				continue
			}
			parseJSONObjects(l, val, remark, tabCount)
		default:
			fmt.Println(reflect.TypeOf(val))
			continue
		}
	}
	return dataTypes, level
}
