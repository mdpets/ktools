package tools

import (
	"io/ioutil"
	"log"
	"os"
)

// ReadContents reads the file and returns the content of the file.
func ReadContents(fileName string) []byte {
	fileReader, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer fileReader.Close()
	content, err := ioutil.ReadAll(fileReader)
	if err != nil {
		log.Fatal(err)
	}
	return content
}
