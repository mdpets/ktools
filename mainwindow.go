package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

type MyMainWindow struct {
	*walk.MainWindow
	inTE  *walk.TextEdit
	outTE *walk.TextEdit
}

func createMainWindow() *MyMainWindow {
	mw := &MyMainWindow{}
	err := MainWindow{
		AssignTo: &mw.MainWindow,
		Title:    mainTitle,
		MinSize:  Size{Width: 600, Height: 400},
		MenuItems: []MenuItem{
			Menu{
				Text: "文件",
				Items: []MenuItem{
					Action{
						Text: "打开文件",
						Shortcut: Shortcut{ //定义快捷键后会有响应提示显示
							Modifiers: walk.ModControl,
							Key:       walk.KeyO,
						},
						OnTriggered: mw.openFileActionTriggered, //点击动作触发响应函数
					},
					Action{
						Text: "另存为",
						Shortcut: Shortcut{
							// Modifiers: walk.ModControl | walk.ModShift,
							Modifiers: walk.ModControl,
							Key:       walk.KeyS,
						},
						OnTriggered: mw.saveFileActionTriggered,
					},
					Action{
						Text: "退出",
						OnTriggered: func() {
							mw.Close()
						},
					},
				},
			},
			Menu{
				Text: "工具",
				Items: []MenuItem{
					Action{
						Text: "Json2Struct",
						Shortcut: Shortcut{ //定义快捷键后会有响应提示显示
							Modifiers: walk.ModControl,
							Key:       walk.KeyJ,
						},
						OnTriggered: mw.toolsJson2StructTitle,
					},
					Action{
						Text: "Json格式化",
						Shortcut: Shortcut{ //定义快捷键后会有响应提示显示
							Modifiers: walk.ModControl,
							Key:       walk.KeyF,
						},
						OnTriggered: mw.toolsJsonFormatTitle,
					},
					Action{
						Text: "手机号归属地",
						Shortcut: Shortcut{ //定义快捷键后会有响应提示显示
							Modifiers: walk.ModControl,
							Key:       walk.KeyM,
						},
						OnTriggered: mw.toolsMobileTitle,
					},
				},
			},
			Menu{
				Text: "帮助",
				Items: []MenuItem{
					Action{
						Text:        "版本",
						OnTriggered: mw.release,
					},
					Action{
						Text:        "关于",
						OnTriggered: mw.about,
					},
				},
			},
		},
		// ToolBar: ToolBar{ //工具栏
		// 	ButtonStyle: ToolBarButtonTextOnly,
		// 	Items: []MenuItem{
		// 		Menu{
		// 			Text: "New",
		// 			Items: []MenuItem{
		// 				Action{
		// 					Text:        "A",
		// 					OnTriggered: mw.newAction_Triggered,
		// 				},
		// 				Action{
		// 					Text:        "B",
		// 					OnTriggered: mw.newAction_Triggered,
		// 				},
		// 			},
		// 			// OnTriggered: mw.newAction_Triggered, //在菜单中不可如此定义，会无响应
		// 		},
		// 		Separator{}, //分隔符
		// 		Action{
		// 			Text:        "View",
		// 			OnTriggered: mw.changeViewAction_Triggered,
		// 		},
		// 	},
		// },
		Layout: VBox{
			Alignment: AlignHCenterVCenter,
			Spacing:   5,
		},
		Children: []Widget{
			GroupBox{
				Title:  "主窗口",
				Layout: VBox{},
				Children: []Widget{
					HSplitter{ //局部水平排列的控件们
						Children: []Widget{
							TextEdit{AssignTo: &mw.inTE, HScroll: true, VScroll: true, MaxLength: 100000000},
							TextEdit{AssignTo: &mw.outTE, Text: GetMainText(), TextColor: walk.RGB(47, 79, 79), ReadOnly: true, HScroll: true, VScroll: true},
						},
					},
				},
			},

			Composite{
				Layout:    HBox{},
				Alignment: AlignHCenterVCenter,
				Children: []Widget{
					HSpacer{},
					PushButton{
						MaxSize:     Size{Width: 60, Height: 40},
						Text:        "执行",
						ToolTipText: "Click to run.",
						Font:        Font{Family: "微软雅黑", PointSize: 14},
						OnClicked:   mw.exchange,
						Alignment:   AlignHCenterVCenter,
					},
					HSpacer{},
				},
			},
		},
		OnDropFiles: mw.dropFiles, //放置文件事件响应函数
	}.Create()

	if err != nil {
		log.Fatalln(err)
	}
	// icon, err := walk.NewIconFromFileWithSize("assets/img/logo.png", walk.Size{Width: 16, Height: 16})
	// icon, _ := walk.NewIconFromFile("assets/img/logo.png")
	// if err == nil {
	// mw.SetIcon(icon)
	// } else {
	// fmt.Println(err)
	// }

	return mw
}

func GetMainText() string {
	text := "Welcome to KTools .\r\n"
	text = fmt.Sprintf("%s\r\n工具一：文件操作", text)
	text = fmt.Sprintf("%s\r\n\t包括文件打开、读取、修改、另存为。\r\n", text)
	text = fmt.Sprintf("%s\r\n工具二：Json2Struct【默认】", text)
	text = fmt.Sprintf("%s\r\n\t支持带注释的JSON转Struct结构。\r\n", text)
	text = fmt.Sprintf("%s\r\n工具三：JsonFormat", text)
	text = fmt.Sprintf("%s\r\n\t支持带注释的JSON格式化。\r\n", text)
	text = fmt.Sprintf("%s\r\n工具四：手机归属地", text)
	text = fmt.Sprintf("%s\r\n\t批量手机号查询归属地信息。\r\n", text)
	return text
}

func (mw *MyMainWindow) exchange() {
	//转换按钮
	switch btnType {
	case "main":
		// walk.MsgBox(mw, "提示", "请先选择操作工具", walk.MsgBoxIconInformation)
		// outText := mw.outTE.Text()
		// if len(outText) > 0 {
		// 	outText = fmt.Sprintf("%s\r\n%s 请先选择操作工具", outText, time.Now().Format("2006-01-02 15:04:05"))
		// } else {
		// 	outText = fmt.Sprintf("%s 请先选择操作工具", time.Now().Format("2006-01-02 15:04:05"))
		// }
		// mw.outTE.SetText(outText)

		inText := mw.inTE.Text()
		if len(inText) > 0 {
			inText = strings.Trim(inText, " ")
			if inText[0:1] == "{" || inText[0:1] == "[" {
				mw.toolsJson2Struct()
			} else {
				mw.toolsMobile()
			}
		}
		break
	case "toolsJsonFormat":
		mw.toolsJsonFormat()
		break
	case "toolsJson2Struct":
		mw.toolsJson2Struct()
		break
	case "toolsMobile":
		mw.toolsMobile()
		break
	}
}
