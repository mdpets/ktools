package main

import (
	"github.com/lxn/walk"
)

const (
	mainTitle = "KTools"
)

func main() {
	btnType = "main"
	mw := createMainWindow()
	mw.Run()
}

func (mw *MyMainWindow) about() {
	walk.MsgBox(mw, "关于", "Have fun ! Enjoy it !\r\nEmail：mgtv@vip.qq.com", walk.MsgBoxIconInformation)
}

func (mw *MyMainWindow) release() {
	walk.MsgBox(mw, "版本", "release v0.0.1", walk.MsgBoxIconInformation)
}
