package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ktools/phonedata"
	"ktools/tools"
	"strings"
	"time"
)

var btnType string

func (mw *MyMainWindow) toolsJson2StructTitle() {
	title := fmt.Sprintf("%s>Json2Struct", mainTitle)
	mw.MainWindow.SetTitle(title)
	inText := ""
	mw.inTE.SetText(inText)
	mw.outTE.SetText("功能:JSON转Golang Struct，支持带注释的伪JSON。")
	btnType = "toolsJson2Struct"
}

func (mw *MyMainWindow) toolsJsonFormatTitle() {
	title := fmt.Sprintf("%s>JsonFormat", mainTitle)
	mw.MainWindow.SetTitle(title)
	inText := ""
	mw.inTE.SetText(inText)
	mw.outTE.SetText("功能:JSON格式化，支持带注释的伪JSON。")
	btnType = "toolsJsonFormat"
}

func (mw *MyMainWindow) toolsMobileTitle() {
	title := fmt.Sprintf("%s>手机归属地查询", mainTitle)
	mw.MainWindow.SetTitle(title)
	mw.inTE.SetText("")
	mw.outTE.SetText("功能:手机归属地查询，支持批量查询，一行一个手机号")
	btnType = "toolsMobile"
}

func (mw *MyMainWindow) toolsJson2Struct() {
	inText := mw.inTE.Text()
	outText := mw.outTE.Text()
	if len(inText) == 0 {
		if len(outText) > 0 {
			outText = fmt.Sprintf("%s\r\n%s 请在左边输入框输入要处理的数据", outText, time.Now().Format("2006-01-02 15:04:05"))
		} else {
			outText = fmt.Sprintf("%s 请在左边输入框输入要处理的数据", time.Now().Format("2006-01-02 15:04:05"))
		}
		mw.outTE.SetText(outText)
		return
	}
	inText, structList, err := tools.ProcessJson(inText)
	if err != nil {
		if len(outText) > 0 {
			outText = fmt.Sprintf("%s\r\n%s 转换出错：%s", outText, time.Now().Format("2006-01-02 15:04:05"), err.Error())
		} else {
			outText = fmt.Sprintf("%s 转换出错：%v", time.Now().Format("2006-01-02 15:04:05"), err.Error())
		}
		mw.outTE.SetText(outText)
	} else {
		if len(inText) > 0 {
			var str bytes.Buffer
			_ = json.Indent(&str, []byte(inText), "\r\n", "\t")
			mw.inTE.SetText(str.String())
		}
		mw.outTE.SetText(tools.Print(structList))
	}
}

func (mw *MyMainWindow) toolsJsonFormat() {
	inText := mw.inTE.Text()
	outText := mw.outTE.Text()
	if len(inText) == 0 {
		if len(outText) > 0 {
			outText = fmt.Sprintf("%s\r\n%s 请在左边输入框输入要处理的数据", outText, time.Now().Format("2006-01-02 15:04:05"))
		} else {
			outText = fmt.Sprintf("%s 请在左边输入框输入要处理的数据", time.Now().Format("2006-01-02 15:04:05"))
		}
		mw.outTE.SetText(outText)
		return
	}
	outText, err := tools.JsonFormat(inText)
	if err != nil {
		outText = fmt.Sprintf("%s 转换出错：%v", time.Now().Format("2006-01-02 15:04:05"), err.Error())
		mw.outTE.SetText(outText)
	} else {
		mw.outTE.SetText(outText)
	}
}

func (mw *MyMainWindow) toolsMobile() {
	title := fmt.Sprintf("%s>手机归属地查询", mainTitle)
	mw.MainWindow.SetTitle(title)
	btnType = "toolsMobile"
	inText := mw.inTE.Text()
	if len(inText) == 0 {
		outText := fmt.Sprintf("%s 输入手机号为空，一行一个，请正确输入...", time.Now().Format("2006-01-02 15:04:05"))
		mw.outTE.SetText(outText)
		return
	}
	temp := strings.Split(inText, "\r\n")
	outText := "手机归属地查询(一行一个)"
	outText = fmt.Sprintf("%s\r\n..............................................................\r\n", outText)
	for _, def := range temp {
		def := strings.Trim(def, " ")
		if len(def) == 11 {
			pr, err := phonedata.Find(def)
			if err == nil {
				outText = fmt.Sprintf("%s\r\n%s\r\n手机号：%s\r\n地区码：%s\r\n运营商：%s\r\n省份：%s\r\n城市：%s\r\n邮编：%s", outText, time.Now().Format("2006-01-02 15:04:05"), pr.PhoneNum, pr.AreaZone, pr.CardType, pr.Province, pr.City, pr.ZipCode)
				outText = fmt.Sprintf("%s\r\n..............................................................\r\n", outText)
				mw.outTE.SetText(outText)
			}
		} else {
			outText = fmt.Sprintf("%s\r\n%s\r\n错误的手机号：%s\r\n", outText, time.Now().Format("2006-01-02 15:04:05"), def)
			outText = fmt.Sprintf("%s\r\n..............................................................\r\n", outText)
			mw.outTE.SetText(outText)
		}
	}
}
