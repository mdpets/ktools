package main

import (
	"fmt"
	"io"
	"ktools/tools"
	"os"
	"strings"
	"time"

	"github.com/lxn/walk"
)

func (mw *MyMainWindow) openFileActionTriggered() {
	mw.MainWindow.SetTitle(mainTitle)
	dlg := new(walk.FileDialog)
	dlg.Title = "打开文件"
	dlg.Filter = "文本文件 (*.txt)|*.txt|所有文件 (*.*)|*.*"
	outText := mw.outTE.Text()
	if len(outText) > 0 {
		outText = fmt.Sprintf("%s\r\n%s 开始选择要打开的文件...", outText, time.Now().Format("2006-01-02 15:04:05"))
	}
	mw.outTE.SetText(outText)
	if ok, err := dlg.ShowOpen(mw); err != nil {
		outText = fmt.Sprintf("%s\r\n%s 打开文件发生错误:%s", outText, time.Now().Format("2006-01-02 15:04:05"), err.Error())
		mw.outTE.SetText(outText)
		return
	} else if !ok {
		outText = fmt.Sprintf("%s\r\n%s 用户取消操作!", outText, time.Now().Format("2006-01-02 15:04:05"))
		mw.outTE.SetText(outText)
		return
	}
	outText = fmt.Sprintf("%s\r\n%s 选择了：%s", outText, time.Now().Format("2006-01-02 15:04:05"), dlg.FilePath)
	content := tools.ReadContents(dlg.FilePath)
	mw.outTE.SetText(outText)
	mw.inTE.SetText(string(content))
}

func (mw *MyMainWindow) saveFileActionTriggered() {
	mw.MainWindow.SetTitle(mainTitle)
	dlg := new(walk.FileDialog)
	dlg.Title = "另存为"
	outText := mw.outTE.Text()
	outText = fmt.Sprintf("%s\r\n%s 文本另存为操作开始...", outText, time.Now().Format("2006-01-02 15:04:05"))
	mw.outTE.SetText(outText)
	if ok, err := dlg.ShowSave(mw); err != nil {
		outText = fmt.Sprintf("%s\r\n%s 文件另存为操作失败:%s", outText, time.Now().Format("2006-01-02 15:04:05"), err.Error())
		mw.outTE.SetText(outText)
		return
	} else if !ok {
		outText = fmt.Sprintf("%s\r\n%s 用户取消操作!", outText, time.Now().Format("2006-01-02 15:04:05"))
		mw.outTE.SetText(outText)
		return
	}

	data := mw.inTE.Text()
	filename := dlg.FilePath
	f, err := os.Open(filename)
	if err != nil {
		f, _ = os.Create(filename)
	} else {
		f.Close()
		f, err = os.OpenFile(filename, os.O_WRONLY, 0x666)
	}
	if len(data) == 0 {
		f.Close()
		return
	}
	io.Copy(f, strings.NewReader(data))
	f.Close()
	outText = fmt.Sprintf("%s\r\n%s 文件另存为操作成功:%s", outText, time.Now().Format("2006-01-02 15:04:05"), filename)
	mw.outTE.SetText(outText)
}

func (mw *MyMainWindow) dropFiles(files []string) {
	mw.outTE.SetText("")
	for _, v := range files {
		mw.outTE.AppendText(v + "\r\n")
	}
}
