# ktools 小工具 ![Image text](assets/img/logo.png)

## 主要功能

- JSON2Struct
  - JSON转golang struct结构体，支持带注释的JSON
- JSONFormat
  - JSON格式化
- 文件处理
  - 打开文件，修改，另存为
- 手机归属地查询
  - 一行一个手机号，支持批量查询

```bash
# 仅支持windows系统编译使用
go build -ldflags="-H windowsgui"
```

## 初心：方便一点点

- 如果有帮助到你，或您有更好的想法，欢迎拓展

## 软件截图

![Image text](assets/img/sceen1.jpg)

![Image text](assets/img/sceen2.jpg)

![Image text](assets/img/sceen3.jpg)
