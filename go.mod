module ktools

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/akavel/rsrc v0.9.0 // indirect
	github.com/andlabs/libui v0.0.0-20201002142932-f3982618e778 // indirect
	github.com/andlabs/ui v0.0.0-20200610043537-70a69d6ae31e // indirect
	github.com/chenqinghe/redis-desktop v0.1.0-alpha // indirect
	github.com/iancoleman/strcase v0.1.2
	github.com/lxn/walk v0.0.0-20200924155701-77185e9c4aec
	github.com/lxn/win v0.0.0-20201021184640-9c48443b1f44 // indirect
	github.com/mattn/go-gtk v0.0.0-20191030024613-af2e013261f5 // indirect
	github.com/mattn/go-pointer v0.0.1 // indirect
	github.com/therecipe/env_darwin_amd64_513 v0.0.0-20190626001412-d8e92e8db4d0 // indirect
	github.com/therecipe/env_linux_amd64_513 v0.0.0-20190626000307-e137a3934da6 // indirect
	github.com/therecipe/env_windows_amd64_513 v0.0.0-20190626000028-79ec8bd06fb2 // indirect
	github.com/therecipe/env_windows_amd64_513/Tools v0.0.0-20190626000028-79ec8bd06fb2 // indirect
	github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d // indirect
	github.com/therecipe/qt/internal/binding/files/docs/5.12.0 v0.0.0-20200904063919-c0c124a5770d // indirect
	github.com/therecipe/qt/internal/binding/files/docs/5.13.0 v0.0.0-20200904063919-c0c124a5770d // indirect
	golang.org/x/mobile v0.0.0-20200801112145-973feb4309de // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
